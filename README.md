# Technologies

icL has a set of technologies, which permite to make the impossible possible.

List of technologies:
* __icL-ALive__ (always alive) permits to restore the command processor from an
  incorrect state and continue running of program.
* __icL-CE__ (contextual execution) is the theory of stateless command
  processing.
* __icL-CP__ (command processor) is a realization of contextual execution.
* __icL-Crossfire__ permits to call icL functions from web page.
* __icL-Editor__ is a text editor adopted for icL necessities.
* __icL-FlyProgramming__ is the second generation of programming on fly.
* __icL-FlyTime__ is the hybrid analyzer of code (statically and dynamically).
* __icL-IDE__ (integrated development environment) is a model of tool of
  creation and editing of icL scripts.
* __icL-Report__ collects, analyzes and reports the autotests results.
* __icL-SV__ (serialized values) is a model of working with CSV and TSV files.
* __icL-Share__ is a model of distributed and parallel test running.
* __icL-Sync__ permits to sync asynchronous server request with autotests.
* __icL-VMS__ (virtual machines stack) is a model of data storing.
* __icL-WD__ (web driver) ia a copy of Selenium library written in C++.
